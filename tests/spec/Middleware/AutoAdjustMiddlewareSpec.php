<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\OptimalRateConcurrencyMeasurerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AutoAdjustMiddlewareSpec extends ObjectBehavior
{
    public function it_should_not_adjust_for_non_valid_measurer(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator,
        OptimalRateConcurrencyMeasurerInterface $measurer
    ) {
        $measurer->valid()->willReturn(false);
        $this->beConstructedWith($measurer);
        $measurer->attach($streamer, $response, $generator)->shouldBeCalled();
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalled();
        $streamer->setConcurrency(Argument::any())->shouldNotBeCalled();
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
    }

    public function it_should_adjust_to_optimal(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator,
        OptimalRateConcurrencyMeasurerInterface $measurer
    ) {
        $this->beConstructedWith($measurer);
        $measurer->valid()->willReturn(true);
        $optimalConcurrency = 2;
        $measurer->getOptimalConcurrency()->willReturn($optimalConcurrency);
        $streamer->getConcurrency()->willReturn(1);
        $measurer->attach($streamer, $response, $generator)->shouldBeCalled();
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalled();
        $streamer->setConcurrency($optimalConcurrency)->shouldBeCalled();
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
    }

    public function it_should_adjust_to_unknown_rate_concurrency_with_100_perc_probability(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator,
        OptimalRateConcurrencyMeasurerInterface $measurer
    ) {
        $this->beConstructedWith($measurer, 1);
        $measurer->valid()->willReturn(true);
        $optimalConcurrency = 2;
        $unknownRateNextConcurrency = 3;
        $measurer->getOptimalConcurrency()->willReturn($optimalConcurrency);
        $measurer->getConcurrencyRate($unknownRateNextConcurrency)->willReturn(null);
        $streamer->getConcurrency()->willReturn($optimalConcurrency);
        $measurer->attach($streamer, $response, $generator)->shouldBeCalled();
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalled();
        $streamer->setConcurrency($unknownRateNextConcurrency)->shouldBeCalled();
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
    }

    public function it_should_not_adjust_to_non_optimal_with_0_perc_probability(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator,
        OptimalRateConcurrencyMeasurerInterface $measurer
    ) {
        $this->beConstructedWith($measurer, 0);
        $measurer->valid()->willReturn(true);
        $optimalConcurrency = 2;
        $optimalRate = 5.0;
        $adjustedConcurrency = 3;
        $adjustedRate = 2.5;
        $measurer->getOptimalConcurrency()->willReturn($optimalConcurrency);
        $measurer->getConcurrencyRate($adjustedConcurrency)->willReturn($adjustedRate);
        $measurer->getConcurrencyRate($optimalConcurrency)->willReturn($optimalRate);
        $streamer->getConcurrency()->willReturn($optimalConcurrency);
        $measurer->attach($streamer, $response, $generator)->shouldBeCalled();
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalled();
        $streamer->setConcurrency($adjustedConcurrency)->shouldNotBeCalled();
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
    }
}
