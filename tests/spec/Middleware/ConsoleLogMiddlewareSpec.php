<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\LoggableMiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ConsoleLogMiddlewareSpec extends ObjectBehavior
{
    public function it_should_build_log_message(
        OutputInterface $output,
        StreamerInterface $streamer,
        LoggableMiddlewareInterface $middleware,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $format = 'START {key1} {key2} {missing} concurrency:{concurrency} END';
        $this->beConstructedWith($output, $format);
        $middleware->getLoggableVars()->willReturn([
            'key1' => 'value1',
            'key2' => 'value2',
        ]);
        $streamer->getMiddlewares()->willReturn([
            $middleware,
        ]);

        $streamer->getConcurrency()->willReturn(2);
        $output->write("\x0D\x1B[2K")->shouldBeCalled();
        $output->write('START value1 value2  concurrency:2 END')->shouldBeCalled();
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
    }
}
