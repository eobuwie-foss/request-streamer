<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\MeasurerInterface;
use Eobuwie\RequestStreamer\Middleware\TransferRateMiddleware;
use PhpSpec\ObjectBehavior;

class TransferRateMiddlewareSpec extends ObjectBehavior
{
    public function it_should_return_loggable_vars(
        MeasurerInterface $measurer
    ) {
        $this->beConstructedWith($measurer);
        $rate = 1.0;
        $bytesSpeed = 2048;
        $measurer->getRate()->willReturn($rate);
        $measurer->getSpeed()->willReturn($bytesSpeed);
        $this->getLoggableVars()->shouldReturn([
            'request_rate' => \sprintf(TransferRateMiddleware::REQUEST_RATE_FORMAT, $rate),
            'download_speed' => \sprintf(TransferRateMiddleware::DOWNLOAD_SPEED_FORMAT, $bytesSpeed / 1024),
        ]);
    }
}
