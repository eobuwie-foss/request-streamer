<?php

namespace spec\Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\MeasurerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OptimalRateConcurrencyMeasurerSpec extends ObjectBehavior
{
    public function let(MeasurerInterface $measurer)
    {
        $this->beConstructedWith($measurer);
    }

    public function it_should_return_always_default_optimal_value()
    {
        $this->getOptimalConcurrency()->shouldBeEqualTo(1);
    }

    public function it_should_return_maximal_rate_concurrency(
        StreamerInterface $streamer,
        ResponseInterface $response,
        MeasurerInterface $measurer,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($measurer);
        $measurer->getRate()->willReturn(1, 2);
        $measurer->valid()->willReturn(true);
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalledTimes(2);
        $streamer->getConcurrency()->willReturn(1, 2);

        $measurer->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);

        $measurer->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);

        $this->getOptimalConcurrency()->shouldBe(2);
    }
}
