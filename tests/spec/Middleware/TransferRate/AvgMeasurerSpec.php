<?php

namespace spec\Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AvgMeasurerSpec extends ObjectBehavior
{
    public function it_returns_valid_metrics_without_any_interaction(
        TimerInterface $timer
    ) {
        $this->beConstructedWith($timer);
        $this->getRate()->shouldReturn(0.0);
        $this->getSpeed()->shouldReturn(0.0);
    }

    public function it_returns_valid_metrics_after_interaction(
        TimerInterface $timer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($timer);
        $timer->getTime()->willReturn(1, 5);
        $response->getInfo('header_size')->willReturn(1);
        $response->getInfo('size_download')->willReturn(3);

        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);

        $expectedTime = 4.0;
        $this->getTime()->shouldReturn($expectedTime);
        $this->getRate()->shouldReturn(1.0 / $expectedTime);
        $this->getSpeed()->shouldReturn(4.0 / $expectedTime);
    }
}
