<?php

namespace spec\Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\MeasurerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MinCountValidMeasurerSpec extends ObjectBehavior
{
    public function it_should_validate_requested_minimal_resolve_invoke_count(
        MeasurerInterface $measurer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($measurer, 2);
        $streamer->getConcurrency()->willReturn(2);
        $measurer->valid()->willReturn(true, true);
        $measurer->attach($streamer, $response, $generator)->shouldBeCalledTimes(2);
        $measurer->resolve($streamer, $response, $generator)->shouldBeCalledTimes(2);
        $this->valid()->shouldBeEqualTo(false);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->valid()->shouldBeEqualTo(false);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->valid()->shouldBeEqualTo(true);
    }
}
