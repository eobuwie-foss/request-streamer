<?php

namespace spec\Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MomentaryMeasurerSpec extends ObjectBehavior
{
    public function it_should_return_valid_stats_without_any_interaction()
    {
        $this->getRate()->shouldBeEqualTo(0.0);
        $this->getSpeed()->shouldBeEqualTo(0.0);
        $this->valid()->shouldBeEqualTo(false);
    }

    public function it_should_return_valid_stats_after_attach(
        TimerInterface $timer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $timer->getTime()->willReturn(1.0);
        $this->attach($streamer, $response, $generator);
        $this->getRate()->shouldBeEqualTo(0.0);
        $this->getSpeed()->shouldBeEqualTo(0.0);
        $this->valid()->shouldBeEqualTo(false);
    }

    public function it_should_return_valid_stats_after_resolve(
        TimerInterface $timer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($timer, 1);
        $response->getInfo('header_size')->willReturn(2);
        $response->getInfo('size_download')->willReturn(8);
        $timer->getTime()->willReturn(1, 3);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->getRate()->shouldBeEqualTo(0.5);
        $this->getSpeed()->shouldBeEqualTo(5.0);
        $this->valid()->shouldBeEqualTo(true);
    }

    public function it_should_should_be_resetable_after_resolve(
        TimerInterface $timer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($timer);
        $response->getInfo('header_size')->willReturn(2);
        $response->getInfo('size_download')->willReturn(8);
        $timer->getTime()->willReturn(1, 3);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->reset();
        $this->getRate()->shouldBeEqualTo(0.0);
        $this->getSpeed()->shouldBeEqualTo(0.0);
        $this->valid()->shouldBeEqualTo(false);
    }

    public function it_should_should_be_valid_according_to_window_size(
        TimerInterface $timer,
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->beConstructedWith($timer, 2);
        $response->getInfo('header_size')->willReturn(2);
        $response->getInfo('size_download')->willReturn(8);
        $timer->getTime()->willReturn(1, 3);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->valid()->shouldBeEqualTo(false);
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->valid()->shouldBeEqualTo(true);
    }
}
