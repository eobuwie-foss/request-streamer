<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ResolvedRequestCountMiddlewareSpec extends ObjectBehavior
{
    public function it_should_successfully_attach_response(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->getRequestCount()->shouldReturn(0);
        $this->getResolvedRequestCount()->shouldReturn(0);

        $this->attach($streamer, $response, $generator);

        $this->getRequestCount()->shouldReturn(1);
        $this->getResolvedRequestCount()->shouldReturn(0);
    }

    public function it_should_successfully_resolve_response(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);

        $this->getRequestCount()->shouldReturn(1);
        $this->getResolvedRequestCount()->shouldReturn(1);
    }

    public function it_should_successfully_return_last_full_sequence_of_resolved_requests(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);

        $this->getRequestCount()->shouldReturn(1);
        $this->getResolvedRequestCount()->shouldReturn(1);
        $this->getLastFullSequenceOfResolvedRequests()->shouldReturn(1);
        $this->getLastFullSequenceOfResolvedRequests()->shouldReturn(0);
    }

    public function it_should_successfully_reset(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ) {
        $this->attach($streamer, $response, $generator);
        $this->resolve($streamer, $response, $generator);
        $this->reset();

        $this->getRequestCount()->shouldReturn(0);
        $this->getResolvedRequestCount()->shouldReturn(0);
        $this->getLastFullSequenceOfResolvedRequests()->shouldReturn(0);
    }
}
