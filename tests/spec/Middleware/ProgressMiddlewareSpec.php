<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\CountableRequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ProgressMiddlewareSpec extends ObjectBehavior
{
    public function it_should_return_empty_progress()
    {
        $this->getProgress()->shouldBeEqualTo(0.0);
    }

    public function it_should_return_valid_progress(
        StreamerInterface $streamer,
        ResponseInterface $response,
        CountableRequestGeneratorInterface $generator1,
        CountableRequestGeneratorInterface $generator2
    ) {
        $generator1->count()->willReturn(2);
        $generator2->count()->willReturn(1);
        $this->attach($streamer, $response, $generator1);
        $this->attach($streamer, $response, $generator2);
        $this->resolve($streamer, $response, $generator1);
        $this->resolve($streamer, $response, $generator2);

        $this->getProgress()->shouldBeEqualTo(0.75);
        $this->getLoggableVars()
            ->shouldHaveKeyWithValue('progress', '75.00 [%]')
        ;
    }
}
