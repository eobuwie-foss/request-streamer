<?php

namespace spec\Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GeneratorDelayMiddlewareSpec extends ObjectBehavior
{
    public function it_should_return_empty_delay()
    {
        $this->getDelay()->shouldBeEqualTo(0.0);
    }

    public function it_should_return_valid_delay(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator,
        TimerInterface $timer
    ) {
        $this->beConstructedWith($timer);
        $timer->getTime()->willReturn(2, 5);
        $this->attach($streamer, $response, $generator);
        $this->getDelay()->shouldBeEqualTo(0.0);
        $this->resolve($streamer, $response, $generator);
        $this->getDelay()->shouldBeEqualTo(0.0);
        $this->attach($streamer, $response, $generator);
        $this->getDelay()->shouldBeEqualTo(3.0);
        $this->getLoggableVars()
            ->shouldHaveKeyWithValue('generator_delay', '3000.00 [ms]')
        ;
    }
}
