<?php

namespace spec\Eobuwie\RequestStreamer;

use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\Streamer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class StreamerSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith();
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Streamer::class);
    }

    public function it_allows_to_append_request_generator_and_invokes_generate(
        RequestGeneratorInterface $requestGenerator
    ): void {
        $responses = [
            new MockResponse('...', ['http_code' => 200]),
            new MockResponse('...', ['http_code' => 200]),
        ];

        $requestGenerator->generate()->willYield($responses);
        $this->append($requestGenerator);
        $i = 0;
        foreach ($this->stream(new MockHttpClient($responses)) as $response) {
            ++$i;
            $this->getSize()->shouldReturn(\count($responses) - $i);
        }
    }

    public function it_should_stream_requests(RequestGeneratorInterface $requestGenerator): void
    {
        $responses = [
            new MockResponse('...', ['http_code' => 200]),
        ];

        $httpClient = new MockHttpClient($responses);

        $requestGenerator->generate()
            ->shouldBeCalledOnce()
            ->will(function () use ($httpClient, $responses) {
                foreach ($responses as $response) {
                    yield $httpClient->request('GET', 'http://dummy');
                }
            })
        ;

        $requestGenerator->resolve($this, Argument::type(MockResponse::class))->willReturn(...$responses);

        $this->append($requestGenerator);
        $this->stream($httpClient)->shouldYield($responses);
    }

    public function it_should_stream_requests_from_multiple_generators(
        RequestGeneratorInterface $requestGenerator,
        RequestGeneratorInterface $requestGenerator2
    ): void {
        $responses = [
            new MockResponse('...1', ['http_code' => 200]),
            new MockResponse('...2', ['http_code' => 200]),
        ];
        $httpClient = new MockHttpClient($responses);
        $requests = [
            $httpClient->request('GET', 'http://dummy'),
            $httpClient->request('GET', 'http://dummy'),
        ];

        $requestGenerator->generate()->will(function () use ($requests) {
            yield $requests[0];
        });

        $requestGenerator2->generate()->will(function () use ($requests) {
            yield $requests[1];
        });

        $requestGenerator->resolve($this, $requests[0])
            ->willReturn($responses[0])
            ->shouldBeCalledOnce()
        ;
        $requestGenerator2->resolve($this, $requests[1])
            ->willReturn($responses[1])
            ->shouldBeCalledOnce()
        ;

        $this->append($requestGenerator);
        $this->append($requestGenerator2);
        $this->stream($httpClient)->shouldYield($responses);
    }

    public function it_should_stream_requests_from_generators_sequentially(
        RequestGeneratorInterface $requestGenerator,
        RequestGeneratorInterface $requestGenerator2
    ): void {
        $responses = [
            new MockResponse('...1', ['http_code' => 200]),
            new MockResponse('...2', ['http_code' => 200]),
            new MockResponse('...3', ['http_code' => 200]),
            new MockResponse('...4', ['http_code' => 200]),
        ];
        $httpClient = new MockHttpClient($responses);
        $requests = [
            $httpClient->request('GET', 'http://dummy'),
            $httpClient->request('GET', 'http://dummy'),
            $httpClient->request('GET', 'http://dummy'),
            $httpClient->request('GET', 'http://dummy'),
        ];

        $requestGenerator->generate()->will(function () use ($requests) {
            yield $requests[0];
            yield $requests[1];
        });

        $requestGenerator2->generate()->will(function () use ($requests) {
            yield $requests[2];
            yield $requests[3];
        });

        $requestGenerator->resolve($this, $requests[0])->willReturn($responses[0])->shouldBeCalledOnce();
        $requestGenerator->resolve($this, $requests[1])->willReturn($responses[1])->shouldBeCalledOnce();

        $requestGenerator2->resolve($this, $requests[2])->willReturn($responses[2])->shouldBeCalledOnce();
        $requestGenerator2->resolve($this, $requests[3])->willReturn($responses[3])->shouldBeCalledOnce();

        $this->append($requestGenerator);
        $this->stream($httpClient)->shouldYield(\array_slice($responses, 0, 2));
        $this->append($requestGenerator2);
        $this->stream($httpClient)->shouldYield(\array_slice($responses, 2, 2));
    }

    public function it_should_invoke_middlewares(
        MiddlewareInterface $middleware,
        RequestGeneratorInterface $requestGenerator
    ): void {
        $responses = [
            new MockResponse('...', ['http_code' => 200]),
        ];

        $httpClient = new MockHttpClient($responses);

        $this->beConstructedWith($middleware);

        $middleware->attach($this, Argument::type(MockResponse::class), $requestGenerator)->shouldBeCalledTimes(
            \count($responses),
        );
        $middleware->resolve($this, Argument::type(MockResponse::class), $requestGenerator)->shouldBeCalledTimes(
            \count($responses),
        );

        $requestGenerator->generate()
            ->shouldBeCalledOnce()
            ->will(function () use ($httpClient, $responses) {
                foreach ($responses as $response) {
                    yield $httpClient->request('GET', 'http://dummy');
                }
            })
        ;

        $requestGenerator->resolve(Argument::any(), Argument::any())->willReturn(...$responses);

        $this->append($requestGenerator);
        $this->stream($httpClient)->shouldYield($responses);
    }
}
