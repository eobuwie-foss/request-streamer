<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface StreamerInterface
{
    public function append(RequestGeneratorInterface $generator): void;

    public function attach(RequestGeneratorInterface $generator, ResponseInterface $response): void;

    /**
     * @return iterable|object[]
     */
    public function stream(HttpClientInterface $httpClient, bool $shouldThrowOnTimeout = false): iterable;

    public function setConcurrency(int $concurrency): void;

    public function getConcurrency(): int;

    public function addMiddleware(MiddlewareInterface ...$middlewares): void;

    public function getMiddleware(string $class): MiddlewareInterface;

    public function getSize(): int;

    /**
     * @return MiddlewareInterface[]
     */
    public function getMiddlewares(): array;

    public function clear(): void;
}
