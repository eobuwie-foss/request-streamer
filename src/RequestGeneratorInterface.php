<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface RequestGeneratorInterface
{
    /**
     * @return \Iterator<ResponseInterface>
     */
    public function generate(): \Iterator;

    public function resolve(StreamerInterface $pool, ResponseInterface $response): object;
}
