<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface MiddlewareInterface
{
    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void;

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void;
}
