<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\OptimalRateConcurrencyMeasurerInterface;
use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AutoAdjustMiddleware implements MiddlewareInterface
{
    private OptimalRateConcurrencyMeasurerInterface $measuer;
    private float $adjustToNonOptimalConcurrencyProbability;

    public function __construct(OptimalRateConcurrencyMeasurerInterface $measuer, float $adjustToNonOptimalConcurrencyProbability = 0.01)
    {
        $this->measuer = $measuer;
        $this->adjustToNonOptimalConcurrencyProbability = $adjustToNonOptimalConcurrencyProbability;
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->measuer->attach($streamer, $response, $generator);
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->measuer->resolve($streamer, $response, $generator);
        $this->adjust($streamer);
    }

    protected function adjust(StreamerInterface $streamer): void
    {
        if (!$this->measuer->valid()) {
            return;
        }

        if ($this->measuer->getOptimalConcurrency() !== $streamer->getConcurrency()) {
            $this->adjustFromNonOptimal($streamer);
        } else {
            $this->adjustToOptimal($streamer);
        }
    }

    protected function adjustFromNonOptimal(StreamerInterface $streamer): void
    {
        $streamer->setConcurrency($this->measuer->getOptimalConcurrency());
    }

    protected function adjustToOptimal(StreamerInterface $streamer): void
    {
        $concurrency = $streamer->getConcurrency();
        $adjustedConcurrency = $streamer->getConcurrency() + 1;
        $adjustedConcurrencyRate = $this->measuer->getConcurrencyRate($adjustedConcurrency);
        if (null === $adjustedConcurrencyRate) {
            $streamer->setConcurrency($adjustedConcurrency);

            return;
        }

        $currentRate = $this->measuer->getConcurrencyRate($concurrency);
        if ($currentRate > $adjustedConcurrencyRate && $this->shouldAdjustToNonOptimal()) {
            $streamer->setConcurrency($adjustedConcurrency);
        }
    }

    protected function shouldAdjustToNonOptimal(): bool
    {
        return $this->adjustToNonOptimalConcurrencyProbability >= (\rand(0, 100) / 100);
    }
}
