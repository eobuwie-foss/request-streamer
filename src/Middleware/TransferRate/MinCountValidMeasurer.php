<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MinCountValidMeasurer extends OuterMeasurer
{
    private int $minCount;
    private int $count = 0;
    private int $concurrency = 1;

    public function __construct(MeasurerInterface $measurer, int $minCount = 1)
    {
        $this->minCount = $minCount;
        parent::__construct($measurer);
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->checkConcurency($streamer);
        parent::attach($streamer, $response, $generator);
    }

    private function checkConcurency(StreamerInterface $streamer): void
    {
        if ($this->concurrency !== $streamer->getConcurrency()) {
            $this->count = 0;
            $this->concurrency = $streamer->getConcurrency();
        }
    }

    public function reset(): void
    {
        $this->count = 0;
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        ++$this->count;
        parent::resolve($streamer, $response, $generator);
        $this->checkConcurency($streamer);
    }

    public function valid(): bool
    {
        return parent::valid() && $this->minCount <= $this->count;
    }
}
