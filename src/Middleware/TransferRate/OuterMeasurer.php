<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class OuterMeasurer implements OuterMeasurerInterface
{
    protected MeasurerInterface $measurer;

    public function __construct(MeasurerInterface $measurer)
    {
        $this->measurer = $measurer;
    }

    public function getInnerMeasurer(): MeasurerInterface
    {
        return $this->measurer;
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->measurer->attach($streamer, $response, $generator);
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->measurer->resolve($streamer, $response, $generator);
    }

    public function getRate(): float
    {
        return $this->measurer->getRate();
    }

    public function reset(): void
    {
        $this->measurer->reset();
    }

    public function getSpeed(): float
    {
        return $this->measurer->getSpeed();
    }

    public function valid(): bool
    {
        return $this->measurer->valid();
    }
}
