<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\MicrotimeTimer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MomentaryMeasurer implements MeasurerInterface
{
    private array $bytes = [];
    private array $startTime = [];
    private array $endTime = [];
    private TimerInterface $timer;
    private int $windowSize;

    public function __construct(TimerInterface $timer = null, int $windowSize = 10)
    {
        $this->timer = $timer ?? new MicrotimeTimer();
        $this->windowSize = $windowSize;
    }

    public function getRate(): float
    {
        if (!$this->valid()) {
            return 0.0;
        }

        return \count($this->bytes) / $this->getTime();
    }

    public function reset(): void
    {
        $this->bytes = [];
        $this->startTime = [];
        $this->endTime = [];
    }

    public function getSpeed(): float
    {
        if (!$this->valid()) {
            return 0.0;
        }

        return \array_sum($this->bytes) / $this->getTime();
    }

    private function getTime(): float
    {
        if (empty($this->startTime)) {
            return 0.0;
        }

        return $this->endTime[\count($this->endTime) - 1] - $this->startTime[0];
    }

    public function valid(): bool
    {
        return \count($this->bytes) >= $this->windowSize;
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->startTime[] = $this->timer->getTime();
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->endTime[] = $this->timer->getTime();
        $this->bytes[] = (int) ($response->getInfo('header_size') + $response->getInfo('size_download'));
        $this->crop();
    }

    private function crop(): void
    {
        if (\count($this->bytes) > $this->windowSize) {
            \array_shift($this->startTime);
            \array_shift($this->endTime);
            \array_shift($this->bytes);
        }
    }
}
