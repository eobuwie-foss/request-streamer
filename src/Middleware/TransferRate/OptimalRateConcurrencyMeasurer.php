<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OptimalRateConcurrencyMeasurer extends OuterMeasurer implements OptimalRateConcurrencyMeasurerInterface
{
    private array $concurrencyRates = [];

    public function getOptimalConcurrency(): int
    {
        if (empty($this->concurrencyRates)) {
            return 1;
        }

        $max = \max($this->concurrencyRates);

        return (int) \array_search($max, $this->concurrencyRates, true);
    }

    public function getConcurrencyRate(int $concurrency): ?float
    {
        return $this->concurrencyRates[$concurrency] ?? null;
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        parent::resolve($streamer, $response, $generator);

        if (parent::valid()) {
            $this->concurrencyRates[$streamer->getConcurrency()] = parent::getRate();
        }
    }
}
