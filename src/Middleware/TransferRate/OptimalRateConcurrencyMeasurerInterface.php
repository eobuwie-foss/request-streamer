<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

interface OptimalRateConcurrencyMeasurerInterface extends MeasurerInterface
{
    public function getOptimalConcurrency(): int;

    public function getConcurrencyRate(int $concurrency): ?float;
}
