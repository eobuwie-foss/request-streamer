<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\MicrotimeTimer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AvgMeasurer implements MeasurerInterface
{
    private int $count = 0;
    private int $bytes = 0;
    private ?float $startTime = null;
    private ?float $endTime = null;
    private TimerInterface $timer;

    public function __construct(TimerInterface $timer = null)
    {
        $this->timer = $timer ?? new MicrotimeTimer();
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        if (null === $this->startTime) {
            $this->startTime = $this->timer->getTime();
        }
    }

    public function reset(): void
    {
        $this->startTime = null;
        $this->endTime = null;
        $this->count = 0;
        $this->bytes = 0;
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->endTime = $this->timer->getTime();
        ++$this->count;
        $this->bytes += (int) ($response->getInfo('header_size') + $response->getInfo('size_download'));
    }

    public function valid(): bool
    {
        return null !== $this->startTime && null !== $this->endTime;
    }

    public function getTime(): float
    {
        if (null !== $this->startTime && null !== $this->endTime) {
            return $this->endTime - $this->startTime;
        }

        return 0.0;
    }

    public function getSpeed(): float
    {
        $time = $this->getTime();

        return 0.0 === $time ? 0.0 : $this->bytes / $time;
    }

    public function getRate(): float
    {
        $time = $this->getTime();

        return 0.0 === $time ? 0.0 : $this->count / $time;
    }
}
