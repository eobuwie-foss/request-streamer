<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

interface OuterMeasurerInterface extends MeasurerInterface
{
    public function getInnerMeasurer(): MeasurerInterface;
}
