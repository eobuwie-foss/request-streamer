<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate;

use Eobuwie\RequestStreamer\MiddlewareInterface;

interface MeasurerInterface extends MiddlewareInterface
{
    public function getRate(): float;

    public function reset(): void;

    public function getSpeed(): float;

    public function valid(): bool;
}
