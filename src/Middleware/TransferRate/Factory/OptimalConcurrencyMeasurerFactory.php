<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate\Factory;

use Eobuwie\RequestStreamer\Middleware\TransferRate\MinCountValidMeasurer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\MomentaryMeasurer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\OptimalRateConcurrencyMeasurer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\OptimalRateConcurrencyMeasurerInterface;

class OptimalConcurrencyMeasurerFactory
{
    public function create(int $minCountValid = 10): OptimalRateConcurrencyMeasurerInterface
    {
        return new OptimalRateConcurrencyMeasurer(
            new MinCountValidMeasurer(
                new MomentaryMeasurer(),
                $minCountValid
            )
        );
    }
}
