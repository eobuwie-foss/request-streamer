<?php

namespace Eobuwie\RequestStreamer\Middleware\TransferRate\Timer;

class MicrotimeTimer implements TimerInterface
{
    public function getTime(): float
    {
        return \microtime(true);
    }
}
