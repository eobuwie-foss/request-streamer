<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware\TransferRate\Timer;

interface TimerInterface
{
    public function getTime(): float;
}
