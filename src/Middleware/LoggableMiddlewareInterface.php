<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\MiddlewareInterface;

interface LoggableMiddlewareInterface extends MiddlewareInterface
{
    public function getLoggableVars(): array;
}
