<?php

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\MeasurerInterface;
use Eobuwie\RequestStreamer\Middleware\TransferRate\MomentaryMeasurer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\OuterMeasurer;
use Eobuwie\RequestStreamer\MiddlewareInterface;

class TransferRateMiddleware extends OuterMeasurer implements MiddlewareInterface, LoggableMiddlewareInterface
{
    public const REQUEST_RATE_FORMAT = '%5.1f [#/sec]';
    public const DOWNLOAD_SPEED_FORMAT = '%5.1f [KB/sec]';

    public function __construct(MeasurerInterface $measurer = null)
    {
        parent::__construct($measurer ?? new MomentaryMeasurer());
    }

    public function getLoggableVars(): array
    {
        return [
            'request_rate' => \sprintf(self::REQUEST_RATE_FORMAT, $this->getInnerMeasurer()->getRate()),
            'download_speed' => \sprintf(self::DOWNLOAD_SPEED_FORMAT, $this->getInnerMeasurer()->getSpeed() / 1024),
        ];
    }
}
