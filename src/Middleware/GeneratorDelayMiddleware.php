<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\MicrotimeTimer;
use Eobuwie\RequestStreamer\Middleware\TransferRate\Timer\TimerInterface;
use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GeneratorDelayMiddleware implements MiddlewareInterface, LoggableMiddlewareInterface
{
    private int $count = 0;
    private ?float $start = null;
    private float $time = 0.0;
    private TimerInterface $timer;

    public function __construct(TimerInterface $timer = null)
    {
        $this->timer = $timer ?? new MicrotimeTimer();
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        if (null !== $this->start) {
            ++$this->count;
            $this->time += $this->timer->getTime() - $this->start;
        }
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->start = $this->timer->getTime();
    }

    public function getDelay(): float
    {
        if (0 === $this->count) {
            return 0.0;
        }

        return $this->time / $this->count;
    }

    public function getLoggableVars(): array
    {
        return [
            'generator_delay' => \sprintf('%5.2f [ms]', 1000 * $this->getDelay()),
        ];
    }
}
