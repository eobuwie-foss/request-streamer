<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\CountableRequestGeneratorInterface;
use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ProgressMiddleware implements MiddlewareInterface, LoggableMiddlewareInterface
{
    private \SplObjectStorage $total;
    private \SplObjectStorage $progress;

    public function __construct()
    {
        $this->total = new \SplObjectStorage();
        $this->progress = new \SplObjectStorage();
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        if (!$generator instanceof CountableRequestGeneratorInterface) {
            return;
        }

        if (!$this->total->contains($generator)) {
            $this->total->attach($generator, $generator->count());
            $this->progress->attach($generator, 0);
        }
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        if (!$generator instanceof CountableRequestGeneratorInterface) {
            return;
        }

        if (!$this->progress->contains($generator)) {
            throw new \LogicException('Generator not attached!');
        }

        $progress = $this->progress->offsetGet($generator);
        $this->progress->offsetSet($generator, $progress + 1);
    }

    public function getProgress(): float
    {
        if (0 === $this->progress->count()) {
            return 0.0;
        }

        $progress = [];
        foreach ($this->progress as $generator) {
            $progress[] = $this->progress->offsetGet($generator) / $this->total->offsetGet($generator);
        }

        return \array_sum($progress) / \count($progress);
    }

    public function getLoggableVars(): array
    {
        return [
            'progress' => \sprintf('%5.2f [%%]', $this->getProgress() * 100),
        ];
    }
}
