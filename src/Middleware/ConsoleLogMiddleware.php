<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ConsoleLogMiddleware implements MiddlewareInterface
{
    private OutputInterface $output;
    public const DEFAULT_FORMAT = "Concurrency:{concurrency}\tRate:{request_rate}\tSpeed:{download_speed}\tProgress:{progress}\tGenerator delay:{generator_delay}\tMemory:{memory}";
    private string $format;

    public function __construct(OutputInterface $output, string $format = self::DEFAULT_FORMAT)
    {
        $this->output = $output;
        $this->format = $format;
    }

    private function display(StreamerInterface $streamer): void
    {
        $vars = $this->getVars($streamer);
        $output = \preg_replace_callback('/{(\w+)}/', fn ($match) => $vars[$match[1]] ?? '', $this->format);
        $this->output->write("\x0D\x1B[2K");
        $this->output->write($output);
    }

    private function getVars(StreamerInterface $streamer): array
    {
        $vars = [
            'concurrency' => $streamer->getConcurrency(),
            'memory' => \sprintf('%5.2f [MB]', \memory_get_usage() / 1024 / 1024),
        ];

        foreach ($streamer->getMiddlewares() as $middleware) {
            if ($middleware instanceof LoggableMiddlewareInterface) {
                $vars += $middleware->getLoggableVars();
            }
        }

        return $vars;
    }

    public function attach(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->display($streamer);
    }

    public function resolve(StreamerInterface $streamer, ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        $this->display($streamer);
    }
}
