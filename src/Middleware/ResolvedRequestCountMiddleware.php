<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer\Middleware;

use Eobuwie\RequestStreamer\MiddlewareInterface;
use Eobuwie\RequestStreamer\RequestGeneratorInterface;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ResolvedRequestCountMiddleware implements MiddlewareInterface
{
    private \SplObjectStorage $storage;
    private int $count;
    private int $resolvedCount;
    private array $resolvedRequests;

    public function __construct()
    {
        $this->reset();
    }

    public function attach(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ): void {
        $this->storage->attach($response, ++$this->count);
        $this->resolvedRequests[$this->count] = false;
    }

    public function resolve(
        StreamerInterface $streamer,
        ResponseInterface $response,
        RequestGeneratorInterface $generator
    ): void {
        $offset = $this->storage->offsetGet($response);

        $this->storage->detach($response);

        $this->resolvedRequests[$offset] = true;

        ++$this->resolvedCount;
    }

    public function getRequestCount(): int
    {
        return $this->count;
    }

    public function getResolvedRequestCount(): int
    {
        return $this->resolvedCount;
    }

    public function getLastFullSequenceOfResolvedRequests(): int
    {
        $count = 0;

        foreach ($this->resolvedRequests as $key => $resolvedRequest) {
            if (false === $resolvedRequest) {
                return $count;
            }

            unset($this->resolvedRequests[$key]);

            ++$count;
        }

        return $count;
    }

    public function reset(): void
    {
        $this->storage = new \SplObjectStorage();
        $this->count = $this->resolvedCount = 0;
        $this->resolvedRequests = [];
    }
}
