<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class Streamer implements StreamerInterface
{
    /**
     * @var MiddlewareInterface[]
     */
    private array $middlewares = [];
    /**
     * @var \SplObjectStorage<ResponseInterface>
     */
    private \SplObjectStorage $stream;
    private \AppendIterator $responses;
    private array $generators = [];
    private int $concurrency = 1;

    public function __construct(
        MiddlewareInterface ...$middlewares
    ) {
        $this->stream = new \SplObjectStorage();
        $this->responses = new \AppendIterator();
        $this->addMiddleware(...$middlewares);
    }

    public function addMiddleware(MiddlewareInterface ...$middlewares): void
    {
        foreach ($middlewares as $middleware) {
            $this->middlewares[\get_class($middleware)] = $middleware;
        }
    }

    public function getMiddleware(string $class): MiddlewareInterface
    {
        return $this->middlewares[$class];
    }

    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    public function stream(HttpClientInterface $httpClient, bool $shouldThrowOnTimeout = false): iterable
    {
        $this->responses->rewind();
        $this->adjust();

        while ($resolved = $this->resolve($httpClient, $shouldThrowOnTimeout)) {
            yield $resolved;
        }

        $this->responses = new \AppendIterator();
        $this->generators = [];
    }

    public function setConcurrency(int $concurrency): void
    {
        $this->concurrency = $concurrency;
    }

    public function getConcurrency(): int
    {
        return $this->concurrency;
    }

    public function getSize(): int
    {
        return $this->stream->count();
    }

    public function append(RequestGeneratorInterface $generator): void
    {
        $this->responses->append($generator->generate());
        $this->generators[] = $generator;
    }

    public function attach(RequestGeneratorInterface $generator, ResponseInterface $response): void
    {
        $this->stream->attach($response, $generator);

        foreach ($this->middlewares as $middleware) {
            $middleware->attach($this, $response, $generator);
        }
    }

    /**
     * @deprecated
     */
    public function clear(): void
    {
        $this->stream = new \SplObjectStorage();
        $this->responses = new \AppendIterator();
        $this->generators = [];
    }

    private function resolve(HttpClientInterface $httpClient, bool $shouldThrowOnTimeout = false): ?object
    {
        foreach ($httpClient->stream($this->stream) as $response => $chunk) {
            if ($chunk->isTimeout() && $shouldThrowOnTimeout) {
                $response->cancel();
                throw new \InvalidArgumentException($chunk->getError() ?: 'A timeout occured!');
            }

            if (!$chunk->isLast()) {
                continue;
            }

            /** @var RequestGeneratorInterface $generator */
            $generator = $this->stream->offsetGet($response);
            $this->stream->offsetUnset($response);
            $this->resolveWithMiddlewares($response, $generator);
            $object = $generator->resolve($this, $response);
            $this->adjust();

            return $object;
        }

        return null;
    }

    private function resolveWithMiddlewares(ResponseInterface $response, RequestGeneratorInterface $generator): void
    {
        foreach ($this->middlewares as $middleware) {
            $middleware->resolve($this, $response, $generator);
        }
    }

    private function adjust(): void
    {
        while (0 < $this->getUsableConcurrency() && ($response = $this->current())) {
            $this->attach($this->generators[$this->responses->getIteratorIndex()], $response);
            $this->responses->next();
        }
    }

    private function current(): ?ResponseInterface
    {
        return $this->responses->valid() ? $this->responses->current() : null;
    }

    private function getUsableConcurrency(): int
    {
        return \max(0, $this->concurrency - $this->stream->count());
    }
}
