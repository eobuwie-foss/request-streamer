<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

interface ProgressAwareGeneratorInterface extends RequestGeneratorInterface
{
    public function getProgress(): int;
}
