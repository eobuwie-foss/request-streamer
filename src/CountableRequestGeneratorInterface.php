<?php

declare(strict_types=1);

namespace Eobuwie\RequestStreamer;

interface CountableRequestGeneratorInterface extends RequestGeneratorInterface
{
    public function count(): int;
}
