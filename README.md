Request Streamer
================

Library for multiplexing http requests using symfony/http-client. 
It simplifies implementation for streaming response.

Features:

- supports paginated and scrolled resources
- optimal concurrency level select
- console log/progress

Installation
------------
Add package repository to `composer.json`

```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/eobuwie/request-streamer"
        }
    ]
```

Require with composer:

````
composer require eobuwie/request-streamer "~1.0"
````

Usage
------------

Simple paginated resource

```php
<?php

namespace App\Http;

use Eobuwie\RequestStreamer\RequestGeneratorInterface
use Symfony\Component\HttpClient\CurlHttpClient;
use Eobuwie\RequestStreamer\StreamerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Eobuwie\RequestStreamer\CountableRequestGeneratorInterface;
use Eobuwie\RequestStreamer\Streamer;

class SimpleRequestGenerator implements RequestGeneratorInterface, CountableRequestGeneratorInterface
{
    private CurlHttpClient $client;
    private ?int $total;
    private string $uri;

    public function __construct(CurlHttpClient $client, string $uri)
    {
        $this->client = $client;
        $this->uri = $uri;
    }

    public function count(): int
    {
        if (null === $this->total) {
            $this->total = $this->client->request('GET', $this->uri)->toArray()['total'];
        }

        return $this->total;
    }

    public function generate(): \Iterator
    {
        $limit = 10;
        for ($i = 0; $i < \ceil($this->total / $limit); $i++) {
            yield $this->client->request('GET', $this->uri.'?'.http_build_query([
                    'limit' => $limit,
                    'page' => $i,
                ]));
        }
    }

    public function resolve(StreamerInterface $pool, ResponseInterface $response): object
    {
        return (object)$response->toArray();
    }
}

$client = new CurlHttpClient();
$streamer = new Streamer($client);
$streamer->setConcurrency(5);
$streamer->append(new SimpleRequestGenerator($client,'https://example.com/api/resource'));

foreach($streamer->stream() as $response) {
    //handle response
}

```

Setup automatic concurency level control

```php
$client = new CurlHttpClient();
$streamer = new Streamer(
    $client,
    new AutoAdjustMiddleware(
        (new OptimalConcurrencyMeasurerFactory())->create()
    )
);
```
Setup for console command 

```php
$streamer = new Streamer(
    new CurlHttpClient(),
    new GeneratorDelayMiddleware(),
    new AutoAdjustMiddleware((new OptimalConcurrencyMeasurerFactory())->create()),
    new ConsoleLogMiddleware($output),// $output - instance of  Symfony\Component\Console\Output\OutputInterface
    new ProgressMiddleware(),
    new TransferRateMiddleware()
);
```
Example console output
```cli
Concurrency:6   Rate: 30.2 [#/sec]      Speed:  6.0 [KB/sec]    Progress: 5.30 [%]      Generator delay: 0.18 [ms]        Memory: 1.50 [MB]
```
